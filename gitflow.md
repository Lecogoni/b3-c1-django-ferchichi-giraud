# Git Flow

- main : est la branch originelaccueillant la stack initiale et opérationnel.
- dev : tiré depuis main, est la branch qui accueillera les features au fil de leur implémentation, feature realisé sur d'autres branch et  merge dans dev
- feature_name : d'autres branch (portant le nom de la feature implémentée) sont destinées a recevoir le code implémentere; Ces branch sont destinéees à être mergées dans dev.

<br><br>


# Convention pour les commit
 
> un commit est composé de 3 partie : 
>- l’action
>- le nom de la branch entre parenthèse
>- description de ce qui a été fait en anglais précédé de : 
<br><br>

Ex : pour le ticket ma-feature mon commit sera : 

````
git commit -m’feat(ma-feature): descripbe what had been done / what is develop'
````
<br>

### Liste non exhaustive des action possible : 
<br>

- feat : pour le dev de la dite feature
- refacto : pour du réfaction de code
- bugfix : pour la correction de bug
- hotix 
- update
- init

