from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm

from .forms import MemberRegisterForm
from .models import Appointment
from .forms import AppointmentForm
from .models import School


def home(request):
    import requests
    data = {
        'welcomeMsg' : 'hello world, home page', 
     }
    return render(request, 'index.html', context=data)

def profil(request):
    import requests
    user = request.user
    schools = School.objects.filter(user=request.user.id)

    schoolsInfos = []
    for school in schools:
        print(school)
        data = {}
        data['data'] = school
        data['appointments'] = fetchSchoolAppointments(school)
        schoolsInfos.append(data)


    appointments = Appointment.objects.filter(client=request.user.id)
    data = {
        'user': user,
        'schools' : schools,
        'schoolsInfos' : schoolsInfos,
        'appointments' : appointments,
    }
    print(schoolsInfos)
    return render(request, 'profil.html', context=data)



def fetchSchoolAppointments(schoolName):
    return Appointment.objects.filter(school=schoolName)

def member_login(request):
    #import requests

    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            # redirect to a success page
            messages.success(request, ('successfully login') ) 
            return redirect('home')
                    
        else:
            # return invalid login msg
            messages.success(request, ('there was an error login in') ) 
            return redirect('login_member')
    else:
        data = {
            'loginMsg' : 'Login page', 
        }
        return render(request, 'authenticate/login.html', context=data)
    
def member_logout(request):
    logout(request)
    messages.success(request, ('successfully logout'))
    return redirect('home') 

def member_register(request):

    if request.method == "POST":
        form = MemberRegisterForm(request.POST)
        if form.is_valid():
            form.save() 
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']

            user = authenticate(request, username=username, password=password)
            login(request, user)
            # redirect to a success page
            messages.success(request, ('successfully registered') ) 
            return redirect('home')
    else:
        form = MemberRegisterForm()

    return render(request, 'authenticate/register.html', { 'form': form })


def appointments(request):
    
    appointment_list = Appointment.objects.filter(is_booked=False).order_by('date')
    user = User.objects.get(id=request.user.id)
    
    if user.is_staff == True:
        schools = School.objects.filter(user=request.user.id)
        appointment_list = Appointment.objects.filter(school__in = schools).order_by('date')
    else:
        appointment_list = Appointment.objects.filter(is_booked=False).order_by('date')
    
    data = {
        'appointmentMsg' : 'Rendez-vous disponible', 
        'appointments': appointment_list,
        'user_id': user.id,
        'user_is_staff': user.is_staff
     }
    return render(request, 'appointments.html', context=data)

def add_appointment(request,school_id):
    school = School.objects.get(id=school_id)
    form = AppointmentForm(request.POST or None, request.user)
    if request.method == 'POST' and form.is_valid():
        appointment = form.save(commit=False)
        appointment.user = User.objects.get(id=request.user.id)
        appointment.school = school
        try :
            appointment.save()
            return redirect('appointments') # Redirige vers une page de confirmation de la réservation
        except Exception as e:
            return redirect('handler_error') # Redirige vers une page d'erreur

    data = {
        'form': form,
        'school': school
    }
    return render(request, 'reservation.html', context=data)

# Modifier un créneau à partir d'un formulaire rempli avec les nouvelles données du créneau
def modify_appointment(request, appointment_id):
    appointment = Appointment.objects.get(id=appointment_id)
    form = AppointmentForm(request.POST or None, instance=appointment)
    if request.method == 'POST' and form.is_valid():
        appointment = form.save(commit=False)
        appointment.user = request.user
        try :
            appointment.save()
            return redirect('success') # Redirige vers une page des réservations
        except Exception as e:
            return redirect('handler_error') # Redirige vers une page d'erreur

    data = {
        'form': form
    }
    return render(request, 'reservation.html', context=data)

def delete_appointment(request, appointment_id):    
    appointment = Appointment.objects.get(id=appointment_id)
    try :
        appointment.delete()
        return redirect('success') # Redirige vers une page des réservations
    except Exception as e:
        return redirect('handler_error') # Redirige vers une page d'erreur

# Path: reservation/views.py

#from .forms import AppointmentForm
#from .forms import ReservationForm

def book_appointment(request, appointment_id):
    appointment = Appointment.objects.get(id=appointment_id)
    appointment.client = request.user
    appointment.is_booked = True
    appointment.save()
    return redirect('profil')

def  delete_booking(request, appointment_id):
    appointment = Appointment.objects.get(id=appointment_id)
    appointment.client = None
    appointment.is_booked = False
    appointment.save()
    return redirect('profil')

def make_reservation_appointment(request, appointment_id):
    appointment = Appointment.objects.get(id=appointment_id)
    form = ReservationForm(request.POST or None, instance=appointment)
    if request.method == 'POST' and form.is_valid():
        appointment = form.save(commit=False)
        try :
            appointment.save()
            return redirect('appointments') # Redirige vers une page de confirmation de la réservation
        except Exception as e:
            return redirect('handler_error') # Redirige vers une page d'erreur

    data = {
        'form': form,
        'appointment_id': appointment_id
    }
    return render(request, 'make_reservation.html', context=data)

def get_all_my_schools(request):
    schools = School.objects.filter(user=request.user)
    return render(request, 'all_my_schools.html', {'schools': schools})

def get_all_my_appointments(request):
    appointments = Appointment.objects.filter(user=request.user)
    return render(request, 'all_my_appointments.html', {'appointments': appointments})

def get_my_schools(request):
    schools = School.objects.filter(user=request.user)
    return render(request, 'my_schools.html', {'schools': schools})

def get_my_school_appointments(request, school_id):
    appointments = Appointment.objects.filter(school=school_id)
    return render(request, 'my_school_appointments.html', {'appointments': appointments})

def get_client_appointments(request, client_id):
    appointments = Appointment.objects.filter(client=client_id)
    return render(request, 'client_appointments.html', {'appointments': appointments})






# Path: schools/views.py
from .forms import SchoolForm
from django.contrib.auth.models import User

def add_school(request):
    form = SchoolForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        school = form.save(commit=False)
        school.user = User.objects.get(id=request.user.id)
        try :
            school.save()
            return redirect('profil') # Redirige vers une page des écoles
        except Exception as e:
            return redirect('handler_error')

    data = {
        'form': form
    }
    return render(request, 'schools.html', context=data)

def modify_school(request, school_id):
    school = School.objects.get(id=school_id)
    school.name = request.name
    try :
        school.save()
        return redirect('success') # Redirige vers une page des écoles
    except Exception as e:
        return redirect('handler_error')

def delete_school(request, school_id):    
    school = School.objects.get(id=school_id)
    try :
        school.delete()
        return redirect('success') # Redirige vers une page des écoles
    except Exception as e:
        return redirect('handler_error')

# Path: error/views.py

def handler_error(request):
    return render(request, 'error.html', status=404)
