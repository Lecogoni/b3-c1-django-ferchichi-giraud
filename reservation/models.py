# from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
from django.db import models
from django.forms import ModelForm

from datetime import date

# Create your models here.

class School(models.Model):
    name = models.CharField(max_length=200)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=0)
    
    def __str__(self):
        return "%s" % (self.name)

class Appointment(models.Model):
    id = models.AutoField(primary_key=True)
    client = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    school = models.ForeignKey(School, on_delete=models.CASCADE)
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    is_booked = models.BooleanField(default=False)

    def __str__(self):
        return "%s %s %s %s %s %s %s" % (
            self.id, self.client, self.school, 
            self.date, self.start_time.strftime('%H:%M'), self.end_time.strftime('%H:%M'), self.is_booked
        )
