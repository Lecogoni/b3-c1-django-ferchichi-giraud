from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name="home"), 
    path('appointments.html', views.appointments, name="appointments"), 

    #path('reservation.html', views.add_appointment, name="reservation"),
    #path('appointments/<str:appointment_id>', views.make_reservation_appointment, name="make_reservation_appointment"),
    path('add_appointment/<str:school_id>', views.add_appointment, name="add_appointment"),

    path('book_appointment/<str:appointment_id>', views.book_appointment, name="book_appointment"),
    path('delete_booking/<str:appointment_id>', views.delete_booking, name="delete_booking"),
    path('schools.html', views.add_school, name="schools"),
    path('schools.html', views.add_school, name="add_school"),
    path('profil.html', views.profil, name="profil"),
    path('member_login', views.member_login, name="member_login"),
    path('member_logout', views.member_logout, name="member_logout"),
    path('member_register', views.member_register, name="member_register"),
    path('error.html', views.handler_error, name="handler_error"),
]
