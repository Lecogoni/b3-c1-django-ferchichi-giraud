from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from django.db.models.fields import BooleanField

from .models import Appointment
from .models import School

class MemberRegisterForm(UserCreationForm):
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    is_staff = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={'class': 'form-check-input', 'label': 'Je suis une école'}))

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')

    
    def __init__(self, *args, **kwargs):
        super(MemberRegisterForm, self).__init__(*args, **kwargs)

        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['is_staff'].widget.attrs.update({'class': 'form-check-input'})

    def save(self, commit=True):
        user = super(MemberRegisterForm, self).save(commit=False)
        user.is_staff = self.cleaned_data["is_staff"]
        if commit:
            user.save()
        return user


class AppointmentForm(forms.ModelForm):
    class Meta:
        model = Appointment
        fields = ['date', 'start_time', 'end_time']
        widgets = {
            'date': forms.DateInput(attrs={'type': 'date', 'class': 'form-control'}),
            'start_time': forms.TimeInput(attrs={'type': 'time', 'class': 'form-control'}),
            'end_time': forms.TimeInput(attrs={'type': 'time', 'class': 'form-control'}),
        }

        def __init__(self, user, *args, **kwargs):
            super(AppointmentForm, self).__init__(*args, **kwargs)

            # Filtrer les options par l'utilisateur connecté
            self.fields['school'].queryset = School.objects.filter(user=user) 
        
        def save(self, commit=True):
            user = super().save(commit=False)
            user.is_staff = self.cleaned_data.get('is_staff')
            if commit:
                user.save()
            return user

# Formulaire de réservation d'un créneau, utilisé dans la vue make_reservation_appointment,
# pour compléter les informations d'un rendez-vous existant avec les informations du client
# class ReservationForm(forms.ModelForm):
    # class Meta:
    #     model = Appointment
    #     fields = ['client', 'client_email', 'client_phone']
    #     widgets = {
    #         'client_name': forms.TextInput(attrs={'class': 'form-control'}),
    #         'client_email': forms.EmailInput(attrs={'class': 'form-control'}),
    #         'client_phone': forms.TextInput(attrs={'class': 'form-control'})
    #     }
    #     client_name = forms.CharField(required=True ,max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    #     client_email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={'class': 'form-control'}))
    #     client_phone = forms.CharField(required=True, max_length=20, widget=forms.TextInput(attrs={'class': 'form-control'}))
       
class SchoolForm(forms.ModelForm):
    class Meta:
        model = School
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'})
        }
        name = forms.CharField(required=True ,max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))