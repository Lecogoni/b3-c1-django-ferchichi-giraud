# ATTENTE 

### Projet en Groupe : 2 - 3 personnes
<br>

> Nom du repo: b3-cx-django-nom-nom-nom
<br>

### Créez une application de gestion des réservations pour les écoles de pilotage.
<br>

Les utilisateurs peuvent réserver un événement, voir les détails de leur réservation et annuler leur réservation si nécessaire.
- La gestion git du projet sera prise en compte.
- Les comptes utilisateurs et les écoles de pilotages.. pourront être créé depuis le back office
- La création de compte via un formulaire est un bonus
- L’app devra comporter a minima les vues suivantes :
    - Connexion
    - Vue home listant toutes les écoles disponibles à la réservation
    - Vue de réservation clients
    - Vue “École” récapitulant les réservations prises
<br><br>


# STRUCTURATION DES DONNEES


- 1 utilisateur peut avoir plusieurs réservation, plusieurs événement
- 1 école peut avoir plusieurs événements, moment disponible à la réservation
- 1 event appartient a une et une seul école. 1 event peut avoir un participant, 1 user


## • Tables
---

>user
<br>

| field | Description |
| ----------- | ----------- |
| email | Varchar |
| password | Varchar |
| firstname | Varchar |
| lastname | Varchar |


>school
<br>

| field | Description |
| ----------- | ----------- |
| name | Varchar |


>event
<br>

| field | Description |
| ----------- | ----------- |
| name | Varchar |
| startDate | datefield |
| endDate | datefield |
| info | textfield |
| isBooked | boolean, default false |
| school | school.ForeignKey |
| user | user.ForeignKey |


# FRONT VIEW RECAP


- Liste des event disponible
- Etq user connect, vue de mes réservation
- Etq school, vue de mes réservation
- login
- 1 page pour réserver, Etq que user connect j'arrive sur cette page après avoir clic sur un event disponible